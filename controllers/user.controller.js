const userModel = require('../models/user.model')
const isEmail = require('validator/lib/isEmail')
const md5 = require('md5')
const jwt = require('jsonwebtoken')
const fs = require('fs')
var secretKey = fs.readFileSync('private.key')

var message = {
  status: false,
  err_code: 0,
  message: null
}

module.exports = userController = (client) => {
  client.subscribe('user/authenticate')
  client.subscribe('user/createUser')
  client.subscribe('user/login')
  client.subscribe('user/updateUser')
  client.subscribe('user/getInfoUser')

  client.on('message', (topic, message) => {
    switch (topic) {
      case 'user/authenticate':
        authenticate(message, client)
        break
      case 'user/createUser':
        createUser(message, client)
        break
      case 'user/login':
        login(message, client)
        break
      case 'user/updateUser':
        updateUser(message, client)
        break
      case 'user/getInfoUser':
        getInfoUser(message, client)
    }
  })
}

function authenticate(datas, client) {
  try {
    jwt.verify(datas.toString(), secretKey, (err, decoded) => {
      if (err) {
        message = {
          status: false,
          err_code: 500,
          message: 'Something wrong :) Retry Please. If you see this message many times. Inbox my Facebook (fb.com/ttnghia195) for support :v'
        }        
        client.publish('user/authenticated', JSON.stringify(message))
      } else {
        message = {
          status: true,
          err_code: 0,
          message: 'Authenticate success!!!'
        }        
        client.publish('user/authenticated', JSON.stringify(message))
      }
    })
  } catch (error) {
    
  }
}

function createUser(datas, client) {
  try {
    var user = new userModel(JSON.parse(datas))
    user.user_password = md5(user.user_password)
    if (!isEmail(user.user_email)) {
      message = {
        status: false,
        err_code: 1,
        message: 'Email invalidate'
      }
      client.publish('user/createdUser', JSON.stringify(message))
    } else {
      userModel.find({user_email: user.user_email}, (err, doc) => {
        if (err) {
          message = {
            status: false,
            err_code: 500,
            message: 'Something wrong :) Retry Please. If you see this message many times. Inbox my Facebook (fb.com/ttnghia195) for support :v'
          }        
          client.publish('user/createdUser', JSON.stringify(message))
        } else if (doc.length != 0) {
          message = {
            status: false,
            err_code: 1,
            message: 'Your email already exists'
          }
          client.publish('user/createdUser', JSON.stringify(message))
        } else {
          user.save((err) => {
            if (err) {
              message = {
                status: false,
                err_code: 500,
                message: 'Something wrong :) Retry Please. If you see this message many times. Inbox my Facebook (fb.com/ttnghia195) for support :v'
              }            
              client.publish('user/createdUser', JSON.stringify(message))
            } else {
              message = {
                status: true,
                err_code: 0,
                message: user
              }
              client.publish('user/createdUser', JSON.stringify(message))
            }
          })
        }
      })
    }
  } catch (error) {
    
  }
}

function login(datas, client) {
  try {
    var datas = JSON.parse(datas)
    if (!isEmail(datas.user_email)) {
      message = {
        status: false,
        err_code: 1,
        message: 'Email invalidate'
      }
      client.publish('user/confirmLogin', JSON.stringify(message))
    } else {
      userModel.findOne({user_email: datas.user_email}, (err, doc) => {
        if (err) {
          message = {
            status: false,
            err_code: 500,
            message: 'Something wrong :) Retry Please. If you see this message many times. Inbox my Facebook (fb.com/ttnghia195) for support :v'
          }  
          client.publish('user/confirmLogin', JSON.stringify(message))
        } else if (doc == null) {
          message = {
            status: false,
            err_code: 2,
            message: 'You has enter a email does not exits. Try again :)'
          }
          client.publish('user/confirmLogin', JSON.stringify(message))
        } else {
          if (doc.user_password !== md5(datas.user_password)) {
            message = {
              status: false,
              err_code: 2,
              message: 'You has enter a wrong password. Try again :)'
            }
            client.publish('user/confirmLogin', JSON.stringify(message))
          } else {
            var user = {
              user_fullName: doc.user_fullName,
              user_email: doc.user_email,
              user_role: doc.user_role
            }
  
            var token = jwt.sign(user, secretKey, { expiresIn: 60 * 60 * 24 })
  
            message = {
              status: true,
              err_code: 0,
              message: token
            }
            client.publish('user/confirmLogin', JSON.stringify(message))
          }
        }
      })
    }
  } catch (error) {
    
  }
}

function updateUser(datas, client) {
  try {
    var userInfo = new userModel(JSON.parse(datas))
    userInfo.save((err) => {
      if (err) {
        message = {
          status: false,
          err_code: 500,
          message: 'Something wrong :) Retry Please. If you see this message many times. Inbox my Facebook (fb.com/ttnghia195) for support :v'
        }  
        client.publish('user/updatedUser', JSON.stringify(message))
      } else {
        message = {
          status: true,
          err_code: 0,
          message: userInfo
        }  
        client.publish('user/updatedUser', JSON.stringify(message))
      }
    })
  } catch (error) {
    
  }
}

function getInfoUser(datas, client) {
  try {
    datas = JSON.parse(datas)
    userModel.findOne({user_email: datas.user_email}, (err, doc) => {
      if (err) {
        message = {
          status: false,
          err_code: 500,
          message: 'Something wrong :) Retry Please. If you see this message many times. Inbox my Facebook (fb.com/ttnghia195) for support :v'
        }  
        client.publish('user/infoUser', JSON.stringify(message))
      } else {
        message = {
          status: true,
          err_code: 0,
          message: doc
        }  
        client.publish('user/infoUser', JSON.stringify(message))
      }
    })
  } catch (error) {
    
  }
}