const regionModel = require("../models/region.model")

var message = {
  status: false,
  err_code: 0,
  message: null
}

module.exports = regionController = (client) => {
  client.subscribe('region/createRegion')
  client.subscribe('region/updateRegion')
  client.subscribe('region/deleteRegion')
  client.subscribe('region/getRegionById')
  client.subscribe('region/getRegionByWard')
  client.subscribe('region/getAllRegion')

  client.on('message', (topic, message) => {
    switch (topic) {
      case 'region/createRegion':
        createRegion(JSON.parse(message), client)
        break
      case 'region/updateRegion':
        updateRegion(JSON.parse(message), client)
        break
      case 'region/deleteRegion':
        deleteRegion(message, client)
        break
      case 'region/getRegionById':
        getRegionById(message, client)
        break
      case 'region/getRegionByWard':
        getRegionByWard(message,client)
        break
      case 'region/getAllRegion':
        getAllRegion(client)
        break
    }
  })
}

function createRegion (data, client) {
  try {
    data = new regionModel (data)
    countRegion((c) => {
      data.region_id = c.toString()
      data.save((err) => {
        if (err) {
          message = {
            status: false,
            err_code: 500,
            message: 'Something wrong :) Retry Please. If you see this message many times. Inbox my Facebook (fb.com/ttnghia195) for support :v'
          }
          client.publish('region/createdRegion', JSON.stringify(message))
        } else {
          message = {
            status: true,
            err_code: 200,
            message: data
          }
          client.publish('region/createdRegion', JSON.stringify(message))
        }
      })
    })
  } catch (error) {
    
  }
}

function updateRegion (data, client) {
  try {
    regionModel.findOneAndUpdate({region_id: data.region_id}, data, (err, doc) => {
      if (err) {
        message = {
          status: false,
          err_code: 500,
          message: 'Something wrong :) Retry Please. If you see this message many times. Inbox my Facebook (fb.com/ttnghia195) for support :v'
        }
        client.publish('region/updatedRegion',JSON.stringify(message))
      } else {
        message = {
          status: true,
          err_code: 200,
          message: doc
        }
        client.publish('region/updatedRegion', JSON.stringify(message))
      }
    })
  } catch (error) {
    
  }
}

function deleteRegion (regionId, client) {
  try {
    regionModel.deleteOne({region_id: regionId}, (err) => {
      if (err) {
        message = {
          status: false,
          err_code: 500,
          message: 'Something wrong :) Retry Please. If you see this message many times. Inbox my Facebook (fb.com/ttnghia195) for support :v'
        }
        client.publish('region/deletedRegion', JSON.stringify(message))
      } else {
        message = {
          status: true,
          err_code: 200,
          message: 'Deleted'
        }
        client.publish('region/deletedRegion', JSON.stringify(message))
      }
    })
  } catch (error) {
    
  }
}

function getRegionById (regionId, client) {
  try {
    regionId = JSON.parse(regionId)
    regionId = regionId.toString()
    regionModel.findOne({region_id: regionId}, (err, doc) => {
      if (err) {
        message = {
          status: false,
          err_code: 500,
          message: 'Something wrong :) Retry Please. If you see this message many times. Inbox my Facebook (fb.com/ttnghia195) for support :v'
        }
        client.publish('region/region', JSON.stringify(message))
      } else {
        message = {
          status: true,
          err_code: 200,
          message: doc
        }
        client.publish('region/region', JSON.stringify(message))
      }
    })
  } catch (error) {
    
  }
}

function getRegionByWard (wardId, client) {
  try {
    regionModel.find({ward_id: wardId}, (err, doc) => {
      if (err) {
        message = {
          status: false,
          err_code: 500,
          message: 'Something wrong :) Retry Please. If you see this message many times. Inbox my Facebook (fb.com/ttnghia195) for support :v'
        }
        client.publish('region/allRegion', JSON.stringify(message))
      } else {
        message = {
          status: true,
          err_code: 0,
          message: doc
        }
        client.publish('region/allRegion', JSON.stringify(message))
      }
    })
  } catch (error) {
    
  }
}

function getAllRegion (client) {
  try {
    regionModel.find((err, doc) => {
      if (err) {
        message = {
          status: false,
          err_code: 500,
          message: 'Something wrong :) Retry Please. If you see this message many times. Inbox my Facebook (fb.com/ttnghia195) for support :v'
        }
        client.publish('region/allRegion', JSON.stringify(message))
      } else {
        message = {
          status: true,
          err_code: 0,
          message: doc
        }
        client.publish('region/allRegion', JSON.stringify(message))
      }
    })
  } catch (error) {
    
  }
}

function countRegion (callback) {
  try {
    regionModel.findOne({}, null, {sort: {_id: -1}}, (err, doc) => {
      callback(parseInt(doc.region_id) + 1)
    })
  } catch (error) {
    
  }
}