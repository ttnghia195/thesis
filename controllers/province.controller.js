const provinceModel = require('../models/province.model')

module.exports = porvinceController = (client) => {
  client.subscribe('province/getAll')
  client.subscribe('province/getByName')
  client.subscribe('province/getById')

  client.on('message', (topic, message) => {
    switch (topic) {
      case 'province/getAll':
        getAll(client)
        break
      case 'province/getByName':
        getByName(message, client)
        break
      case 'province/getById':
        getById(message, client)
        break
    }
  })
}

function getAll (client) {
  try {
    provinceModel.find({}, (err, doc) => {
      if (!err) {
        doc = JSON.stringify(doc)
        client.publish('province/allProvince', doc)
      }
    })
  } catch (error) {
    
  }
}

function getByName (name, client) {
  try {
    provinceModel.findOne({province_name: name}, (err, doc) => {
      if (!err) {
        doc = JSON.stringify(doc)
        client.publish('province/province', doc)
      }
    })  
  } catch (error) {
    
  }
}

function getById (id, client) {
  try {
    provinceModel.findOne({province_id: id}, (err, doc) => {
      if (!err) {
        doc = JSON.stringify(doc)
        client.publish('province/province', doc)
      }
    })  
  } catch (error) {
    
  }
}
