const stationModel = require('../models/station.model')
const createValidator = require('is-my-json-valid')
const stationSchema = require('../schemas/station')

var message = {
  status: false,
  err_code: 0,
  message: null
}

module.exports = stationController = (client) => {
  client.subscribe('station/createStation')
  client.subscribe('station/updateStation')
  client.subscribe('station/deleteStation')
  client.subscribe('station/getAllStation')
  client.subscribe('station/getStationById')
  client.subscribe('station/getStationBySink')
  client.subscribe('station/getStationByRegion')
  client.subscribe('station/getStationByFarm')

  client.on('message', (topic, message) => {
    switch (topic) {
      case 'station/createStation':
        createStation(message, client)
        break
      case 'station/updateStation':
        updateStation(message, client)
        break
      case 'station/deleteStation':
        deleteStation(message, client)
        break
      case 'station/getAllStation':
        getAllStation(client)
        break
      case 'station/getStationById': 
        getStationById(message, client)
        break
      case 'station/getStationBySink':
        getStationBySink(message, client)
        break
      case 'station/getStationByRegion':
        getStationByRegion(message, client)
        break
      case 'station/getStationByFarm':
        getStationByFarm(message, client)
        break
    }
  })
}

function createStation (datas, client) {
  try {
    try {
      data = JSON.parse(datas)
    } catch (error) {
      message = {
        status: false,
        err_code: 500,
        message: 'Data not match JSON format'
      }
      client.publish('station/createdStation', JSON.stringify(message))
    }

    var validator = createValidator(stationSchema)
    
    if (!validator(data)) {
      message = {
        status: false,
        err_code: 500,
        message: validator.errors
      }
      client.publish('station/createdStation', JSON.stringify(message))
    } else {
      data = new stationModel (data)
      countStation((c) => {
        data.station_id = c.toString()
        data.save((err) => {
          if (err) {
            message = {
              status: false,
              err_code: 500,
              message: 'Something wrong :) Retry Please. If you see this message many times. Inbox my Facebook (fb.com/ttnghia195) for support :v'
            }
            client.publish('station/createdStation', JSON.stringify(message))
          } else {
            message = {
              status: true,
              err_code: 200,
              message: data
            }
            client.publish('station/createdStation', JSON.stringify(message))
          }
        })
      })
    }
  } catch (error) {
    
  }
}

function updateStation (datas, client) {
  try {
    try {
      data = JSON.parse(datas)
    } catch (error) {
      message = {
        status: false,
        err_code: 500,
        message: 'Data not match JSON format'
      }
      client.publish('station/updatedStation', JSON.stringify(message))
    }
    stationModel.findOneAndUpdate({station_id: data.station_id}, data, (err, doc) => {
      if (err) {
        message = {
          status: false,
          err_code: 500,
          message: 'Something wrong :) Retry Please. If you see this message many times. Inbox my Facebook (fb.com/ttnghia195) for support :v'
        }
        client.publish('station/updatedStation', JSON.stringify(message))
      } else {
        message = {
          status: true,
          err_code: 200,
          message: data
        }
        client.publish('station/updatedStation', JSON.stringify(message))
      }
    })
  } catch (error) {
    
  }
}

function deleteStation (stationId, client) {
  try {
    stationModel.deleteOne({station_id: stationId}, (err) => {
      if (err) {
        message = {
          status: false,
          err_code: 500,
          message: 'Something wrong :) Retry Please. If you see this message many times. Inbox my Facebook (fb.com/ttnghia195) for support :v'
        }
        client.publish('station/deletedStation', JSON.stringify(message))
      } else {
        message = {
          status: true,
          err_code: 200,
          message: 'Deleted'
        }
        client.publish('station/deletedStation', JSON.stringify(message))
      }
    })
  } catch (error) {
    
  }
}

function getAllStation (client) {
  try {
    stationModel.find((err, doc) => {
      if (err) {
        message = {
          status: false,
          err_code: 500,
          message: 'Something wrong :) Retry Please. If you see this message many times. Inbox my Facebook (fb.com/ttnghia195) for support :v'
        }
        client.publish('station/allStation', JSON.stringify(message))
      } else {
        message = {
          status: true,
          err_code: 0,
          message: doc
        }
        client.publish('station/allStation', JSON.stringify(message))
      }
    })
  } catch (error) {

  }
}

function getStationById (stationId, client) {
  try {
    stationModel.findOne({station_id: stationId}, (err, doc) => {
      if (!err) {
        doc = JSON.stringify(doc)
        client.publish('station/station', doc)
        getDeviceOnStation(stationId, client)
      }
    })
  } catch (error) {
    
  }
}

function getDeviceOnStation (stationId, client) {
  try {
    deviceModel.find({station_id: stationId}, (err, doc) => {
      if (!err) {
        doc = JSON.stringify(doc)
        client.publish('station/device', doc)
      }
    })
  } catch (error) {
    
  }
}

function getStationBySink (sinkId, client) {
  try {
    stationModel.find({sink_id: sinkId}, (err, doc) => {
      if (!err) {
        doc = JSON.stringify(doc)
        client.publish('station/allStation', doc)
      }
    })
  } catch (error) {
    
  }
}

function getStationByRegion (regionId, client) {
  try {
    stationModel.find({region_id: regionId}, (err, doc) => {
      if (!err) {
        doc = JSON.stringify(doc)
        client.publish('station/allStation', doc)
      }
    })
  } catch (error) {
    
  }
}

function getStationByFarm (farmID, client) {
  try {
    stationModel.find({farm_id: farmID}, (err, doc) => {
      if (!err) {
        doc = JSON.stringify(doc)
        client.publish('station/allStation', doc)
      }
    })
  } catch (error) {
    
  }
}

function countStation (callback) {
  try {
    stationModel.findOne({}, null, {sort: {_id:-1}}, (err, doc) => {
      callback(parseInt(doc.station_id) + 1)
    })
  } catch (error) {
    
  }
}