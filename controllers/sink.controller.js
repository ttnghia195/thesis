const sinkModel = require('../models/sink.model')

var message = {
  status: false,
  err_code: 0,
  message: null
}

module.exports = sinkController = (client) => {
  client.subscribe('sink/getSinkByRegion')

  client.on('message', (topic, message) => {
    switch (topic) {
      case 'sink/getSinkByRegion':
        getSinkByRegion(message, client)
        break
    }
  })
}

function getSinkByRegion (regionId, client) {
  try {
    sinkModel.find({region_id: regionId}, (err, doc) => {
      if (err) {
        message = {
          status: false,
          err_code: 500,
          message: 'Something wrong :) Retry Please. If you see this message many times. Inbox my Facebook (fb.com/ttnghia195) for support :v'
        }
        client.publish('sink/allSink', JSON.stringify(message))
      } else {
        message = {
          status: true,
          err_code: 0,
          message: doc
        }
        client.publish('sink/allSink', JSON.stringify(message))
      }
    })
  } catch (error) {
    
  }
}