const districtModel = require('../models/district.model')

module.exports = porvinceController = (client) => {
  client.subscribe('district/getAll')
  client.subscribe('district/getByName')
  client.subscribe('district/getById')

  client.on('message', (topic, message) => {
    switch (topic) {
      case 'district/getAll':
        getAll(message, client)
        break
      case 'district/getByName':
        getByName(message, client)
        break
      case 'district/getById':
        getById(message, client)
        break
    }
  })
}

function getAll (provinceId, client) {
  try {
    districtModel.find({'province_id': provinceId}, (err, doc) => {
      if (!err) {
        doc = JSON.stringify(doc)
        client.publish('district/allDistrict', doc)
      }
    }) 
  } catch (error) {
    
  }
}

function getByName (name, client) {
  try {
    districtModel.findOne({district_name: name}, (err, doc) => {
      if (!err) {
        doc = JSON.stringify(doc)
        client.publish('district/district', doc)
      }
    })  
  } catch (error) {
    
  }
}

function getById (id, client) {
  try {
    districtModel.findOne({district_id: id}, (err, doc) => {
      if (!err) {
        doc = JSON.stringify(doc)
        client.publish('district/district', doc)
      }
    })  
  } catch (error) {
    
  }
}
