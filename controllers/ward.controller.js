const wardModel = require('../models/ward.model')

module.exports = porvinceController = (client) => {
  client.subscribe('ward/getAll')
  client.subscribe('ward/getByName')
  client.subscribe('ward/getById')

  client.on('message', (topic, message) => {
    switch (topic) {
      case 'ward/getAll':
        getAll(message, client)
        break
      case 'ward/getByName':
        getByName(message, client)
        break
      case 'ward/getById':
        getById(message, client)
        break
    }
  })
}

function getAll (districtId, client) {
  try {
    wardModel.find({district_id: districtId}, (err, doc) => {
      if (!err) {
        doc = JSON.stringify(doc)
        client.publish('ward/allWard', doc)
      }
    })
  } catch (error) {
    
  }
}

function getByName (name, client) {
  try {
    wardModel.findOne({ward_name: name}, (err, doc) => {
      if (!err) {
        doc = JSON.stringify(doc)
        client.publish('ward/ward', doc)
      }
    })
  } catch (error) {
    
  }
}

function getById (id, client) {
  try {
    wardModel.findOne({ward_id: id}, (err, doc) => {
      if (!err) {
        doc = JSON.stringify(doc)
        client.publish('ward/ward', doc)
      }
    })
  } catch (error) {
    
  }
}
