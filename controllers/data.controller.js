const dataModel = require('../models/data.model')
const stationModel = require('../models/station.model')
const deviceModel = require('../models/device.model')
const createValidator = require('is-my-json-valid')
const dataSchema = require('../schemas/data')

module.exports = dataController = (client) => {
  client.subscribe('data/insertData')
  client.subscribe('data/getDataByStation')

  client.on('message', (topic, message) => {
    switch (topic) {
      case 'data/insertData':
        saveData(message, client)
        break
      case 'data/getDataByStation':
        getDataByStation(message, client)
        break
    }
  })
}

function saveData(data, client) {
  try {
    try {
      var datas = JSON.parse(data)
    } catch (error) {
      message = {
        status: false,
        err_code: 500,
        message: 'Data not match JSON format'
      }
      client.publish('data/insertedData', JSON.stringify(message))
    }

    var validator = createValidator(dataSchema)

    if (!validator(datas)) {
      message = {
        status: false,
        err_code: 500,
        message: validator.errors
      }
      client.publish('data/insertedData', JSON.stringify(message))
    } else {
      data_value = datas.data_value
      var data = new dataModel({
        station_id: datas.st_id,
        sink_id: datas.s_id,
        farm_id: datas.f_id,
        data_value: {
          nhietdo: data_value.nd,
          doam: data_value.da,
          anhsang: data_value.as,
          mua: data_value.mua
        },
        data_createdDate: Date.now()
      })

      checkDoAm(data).then(values => {
        if (typeof data == 'string') {
          data = JSON.parse(data)
        }

        deviceModel.findOne({
          station_id: data.station_id,
          device_type: "van"
        }, (err, doc) => {
          if (doc != null) {
            var command = {
              id: doc.device_id,
              status: "OFF",
              time: 0,
              SOS: 'MEDIUM'
            }
  
            if (!values) {
              if (doc.device_status == "OFF") {
                command.status = "ON"
                client.publish('station/controlDevice', JSON.stringify(command))
              }
            } else {
              if (doc.device_status == "ON") {
                command.status = "OFF"
                client.publish('station/controlDevice', JSON.stringify(command))
              }
            }
          }
        })
      })

      setTimeOut(data, client)

      data.save((err) => {
        if (err) console.log('Fail')
        else {
          console.log(new Date(Date.now()).toLocaleString('en-EN', {
            timezone: 'UTC +7'
          }) + ': Station ' + data.station_id + ' insert data success')
          var topic = 'data/station/' + data.station_id
          data = JSON.stringify(data)
          client.publish(topic, data, {
            retain: true
          })
        }
      })
    }
  } catch (error) {

  }
}

function getDataByStation(stationId, client) {
  try {
    dataModel.findOne({
      station_id: stationId
    }, null, {
      sort: {
        data_createdDate: -1
      }
    }, (err, doc) => {
      if (!err) {
        doc = JSON.stringify(doc)
        client.publish('data/returnData', doc)
      }
    })
  } catch (error) {

  }
}

function checkDoAm(data) {
  try {
    return new Promise(resolve => {
      stationModel.findOne({
          station_id: data.station_id
     })
      .then(doc => {
        let temp = false 
        if (data.data_value.doam < doc.station_config.doam.min) { 
          temp = true
        }
          resolve(temp)
        })
      })
    } catch (error) {

    }
  }

function setTimeOut(data, client) {
  try {
    stationModel.findOne({
      station_id: data.station_id
    }).then(doc => {
      var timeout = doc.station_config.timeout
      var timeOut = {
        timeout: timeout
      }
      client.publish('station/' + data.station_id + '/setTimeOut', JSON.stringify(timeOut),{retain: true})
    })
  } catch (error) {

  }
}