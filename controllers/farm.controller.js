const farmModel = require('../models/farm.model')

var message = {
  status: false,
  err_code: 0,
  message: null
}

module.exports = farmController = (client) => {
  client.subscribe('farm/createFarm')
  client.subscribe('farm/updateFarm')
  client.subscribe('farm/deleteFarm')
  client.subscribe('farm/getAllFarm')
  client.subscribe('farm/getFarmById')
  client.subscribe('farm/getFarmByRegion')

  client.on('message', (topic, message) => {
    switch (topic) {
      case 'farm/createFarm':
        createFarm(JSON.parse(message), client)
        break
      case 'farm/updateFarm':
        updateFarm(JSON.parse(message), client)
        break
      case 'farm/deleteFarm':
        deleteFarm(message, client)
        break
      case 'farm/getAllFarm':
        getAllFarm(client)
        break
      case 'farm/getFarmById':
        getFarmById(message, client)
        break
      case 'farm/getFarmByRegion':
        getFarmByRegion(message, client)
        break
    }
  })
}

function createFarm (data, client) {
  try {
    data = new farmModel (data)
    countFarm((c) => {
      data.farm_id = c + 1
      data.save((err) => {
        if (err) {
          message = {
            status: false,
            err_code: 500,
            message: 'Something wrong :) Retry Please. If you see this message many times. Inbox my Facebook (fb.com/ttnghia195) for support :v'
          }
          client.publish('farm/createdFarm', message)
        } else {
          message = {
            status: true,
            err_code: 200,
            message: data
          }
          client.publish('farm/createdFarm', message)
        }
      })
    })  
  } catch (error) {
    
  }
}

function updateFarm (data, client) {
  try {
    farmModel.findOneAndUpdate({farm_id: data.farm_id}, data, (err, doc) => {
      if (err) {
        message = {
          status: false,
          err_code: 500,
          message: 'Something wrong :) Retry Please. If you see this message many times. Inbox my Facebook (fb.com/ttnghia195) for support :v'
        }
        client.publish('farm/updatedFarm', message)
      } else {
        message = {
          status: true,
          err_code: 200,
          message: doc
        }
        client.publish('farm/updatedFarm', message)
      }
    })
  } catch (error) {
    
  }
}

function deleteFarm (farmId, client) {
  try {
    farmModel.deleteOne({farm_id: farmId}, (err) => {
      if (err) {
        message = {
          status: false,
          err_code: 500,
          message: 'Something wrong :) Retry Please. If you see this message many times. Inbox my Facebook (fb.com/ttnghia195) for support :v'
        }
        client.publish('farm/deletedFarm', message)
      } else {
        message = {
          status: true,
          err_code: 200,
          message: 'Deleted'
        }
        client.publish('farm/deletedFarm', message)
      }
    })  
  } catch (error) {
    
  }
}

function getAllFarm (client) {
  try {
    farmModel.find({}, (err, doc) => {
      if (err) {
        message = {
          status: false,
          err_code: 500,
          message: 'Something wrong :) Retry Please. If you see this message many times. Inbox my Facebook (fb.com/ttnghia195) for support :v'
        }
        client.publish('farm/allFarm', message)
      } else {
        message = {
          status: true,
          err_code: 0,
          message: doc
        }
        client.publish('farm/allFarm', message)
      }
    })
  } catch (error) {
    
  }
}

function getFarmById (farmId, client) {
  try {
    farmModel.findOne({farm_id: farmId}, (err, doc) => {
      if (err) {
        message = {
          status: false,
          err_code: 500,
          message: 'Something wrong :) Retry Please. If you see this message many times. Inbox my Facebook (fb.com/ttnghia195) for support :v'
        }
        client.publish('farm/farm', message)
      } else {
        message = {
          status: true,
          err_code: 200,
          message: doc
        }
        client.publish('farm/farm', message)
      }
    })  
  } catch (error) {
    
  }
}

function getFarmByRegion (regionId, client) {
  try {
    farmModel.find({region_id: regionId}, (err, doc) => {
      if (err) {
        message = {
          status: false,
          err_code: 500,
          message: 'Something wrong :) Retry Please. If you see this message many times. Inbox my Facebook (fb.com/ttnghia195) for support :v'
        }
        client.publish('farm/allFarm', JSON.stringify(message))
      } else {
        message = {
          status: true,
          err_code: 0,
          message: doc
        }
        client.publish('farm/allFarm', JSON.stringify(message))
      }
    })  
  } catch (error) {
    
  }
}

function countFarm (callback) {
  try {
    farmModel.countDocuments({}, (err, c) => {
      if (!err) {
        callback(c)
      }
    })
  } catch (error) {
    
  }
}