const deviceModel = require('../models/device.model')

module.exports = stationController = (client) => {
  client.subscribe('station/getStatusDevice')
  client.subscribe('station/controlDevice')

  client.on('message', (topic, message) => {
    switch (topic) {
      case 'station/getStatusDevice':
        getStatusDevice(message.toString(), client)
        break
      case 'station/controlDevice':
        controlDevice(message, client)
        break;
    }
  })
}

function getStatusDevice (stationId, client) {
  try {
    deviceModel.find({station_id: stationId}, (err, doc) => {
      if (err) {
        message = {
          status: false,
          err_code: 500,
          message: 'Something wrong :) Retry Please. If you see this message many times. Inbox my Facebook (fb.com/ttnghia195) for support :v'
        }
        client.publish('station/deviceStatus', JSON.stringify(message))
      } else {
        message = {
          status: false,
          err_code: 500,
          message: doc
        }
        client.publish('station/deviceStatus', JSON.stringify(message))
      }
    })
  } catch (error) {
    
  }
}

function controlDevice (command, client) {
  try {
    var command = JSON.parse(command)
    deviceModel.findOne({device_id: command.id}, (err, doc) => {
      if (!err) {
        doc.device_status = command.status
        doc.updatedDate = Date.now()      
        doc.save((err, doc) => {
          if (!err) {
            client.publish('control/send', JSON.stringify(command))
          }
        })
      }
    })
  } catch (error) {
    
  }
}