const EventEmitter = require('events');
const emitter = new EventEmitter()

emitter.setMaxListeners(0)

const mongoose = require('mongoose')

mongoose.connect('mongodb://198.13.36.248:27017/luanvan', { useNewUrlParser: true })
// mongoose.connect('mongodb://localhost:27017/luanvan', { useNewUrlParser: true })
mongoose.connection.once('open', () => {
  console.log('Connected Database')
})

const mqtt = require('mqtt')
const client = mqtt.connect('mqtt://198.13.36.248:1883')
// const client = mqtt.connect('mqtt://localhost:1883')
// const client = mqtt.connect('ws://localhost:9001/mqtt')

const userController = require('./controllers/user.controller')

const dataController = require('./controllers/data.controller')

const regionController = require('./controllers/region.controller')
const sinkController = require('./controllers/sink.controller')
const farmController = require('./controllers/farm.controller')
const stationController = require('./controllers/station.controller')

const controlController = require('./controllers/control.controller')

const provinceController = require('./controllers/province.controller')
const districtController = require('./controllers/district.controller')
const wardController = require('./controllers/ward.controller')

client.on('connect', () => {
  console.log('Connected Broker')
  userController(client)

  dataController(client)

  regionController(client)
  sinkController(client)
  farmController(client)
  stationController(client)

  controlController(client)

  provinceController(client)
  wardController(client)
  districtController(client)
})

// const express = require('express')
// const bodyParser = require('body-parser')
// const api = express()
// const PORT = process.env.PORT || 5000

// api.use(bodyParser.json())

// api.listen(PORT, () => {
//   console.log('Server started')
//   console.log('Listening on ' + PORT)
// })

// api.post('/', (req, res) => {
//   res.send('Listening on ' + PORT )
// })