var mongoose = require('mongoose')
var Schema = mongoose.Schema

var farmSchema = new Schema({
  farm_id: String,
  region_id: String,
  user_id: String,
  farm_width: Number,
  farm_height: Number,
  farm_description: String,
  farm_status: Number,
  farm_location: String,
  farm_address: String,
  farm_latitude: Number,
  farm_longitude: Number,
  isDelete: {type: Boolean, default: false}
})

module.exports = mongoose.model('Farm', farmSchema)