const mongoose = require('mongoose')
const Schema = mongoose.Schema

var userSchema = new Schema({
  user_fullName: {type: String, default: ""},
  user_birthday: {type: Date, default: ""},
  user_phone: {type: String, default: ""},
  user_email: String,
  user_password: String,
  user_address: {type: String, default: ""},
  user_role: {type: Number, default: 1}
})

module.exports = mongoose.model('User', userSchema)