var mongoose = require('mongoose')
var Schema = mongoose.Schema

var provinceSchema = new Schema({
  province_id: String,
  province_name: String,
  province_type: String
})

module.exports = mongoose.model('Province', provinceSchema)