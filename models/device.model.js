var mongoose = require('mongoose')
var Schema = mongoose.Schema

var deviceSchema = new Schema({
  device_id: String,
  station_id: String,
  device_status: String,
  device_type: String,
  device_description: String,
  device_updatedDate: {type: Date, default: Date()}
})

module.exports = mongoose.model('Device', deviceSchema)
