var mongoose = require('mongoose')
var Schema = mongoose.Schema

var dataSchema = new Schema({
  station_id: String,
  datatype_id: String,
  sink_id: String,
  farm_id: String,
  data_value: {},
  data_createdDate: {type: Date, default: Date()},
  data_stationType: {type: Number, default: 0}
})

module.exports = mongoose.model('Data', dataSchema)
