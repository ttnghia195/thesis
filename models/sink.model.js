var mongoose = require('mongoose')
var Schema = mongoose.Schema

var sinkSchema = new Schema({
  sink_id: String,
  region_id: String,
  sink_name: String,
  sink_code: String,
  sink_secret: String,
  sink_location: String,
  sink_status: Number,
  sink_address: String,
  sink_latitude: Number,
  sink_longitude: Number,
  isDelete: {type: Boolean, default: false}
})

module.exports = mongoose.model('Sink', sinkSchema)