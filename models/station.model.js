var mongoose = require('mongoose')
var Schema = mongoose.Schema

var stationSchema = new Schema({
  station_id: String,
  sink_id: String,
  region_id: String,
  farm_id: String,
  station_name: String,
  station_code: String,
  station_location: String,
  station_node: Array,
  station_status: {type: Number, default: 1},
  station_secret: String,
  station_address: String,
  station_duration: Number,
  station_updateStatus: Number,
  station_latitude: Number,
  station_longitude: Number,
  station_config: {
    nhietdo: {
      min: {type: Number, default: 0},
      max: {type: Number, default: 0}
    },
    doam: {
      min: {type: Number, default: 0},
      max: {type: Number, default: 0}
    },
    timeout: {type: Number, default: 5000}
  },
  isDelete: {type: Boolean,default: false}
})

module.exports = mongoose.model('Station', stationSchema)