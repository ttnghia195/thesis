const mongoose = require('mongoose')
const Schema = mongoose.Schema

var wardSchema = new Schema({
  ward_id: String,
  ward_name: String,
  ward_type: String,
  ward_location: String,
  district_id: String
})

module.exports = mongoose.model('Ward', wardSchema)