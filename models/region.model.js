var mongoose = require('mongoose')
var Schema = mongoose.Schema

var regionSchema = new Schema({
  region_id: String,
  region_name: String,
  region_description: String,
  ward_id: String,
  user_id: {type: Array, default: null}
})

module.exports = mongoose.model('Region', regionSchema)