const mongoose = require('mongoose')
const Schema = mongoose.Schema

var districtSchema = new Schema({
  district_id: String,
  district_name: String,
  district_type: String,
  district_location: String,
  province_id: String
})

module.exports = mongoose.model('District', districtSchema)